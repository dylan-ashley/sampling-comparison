#!/usr/bin/env python3

import random


def random_pick(e, list):
    '''
    Randomly samples from list until it finds the index of an element
    e. Returns the number of samplings to find the index of the
    element. Returns None if e is not in list.
    '''
    if e not in list:
        return None
    count = 1
    while True:
        i = random.randrange(len(list))
        if list[i] == e:
            return count
        count += 1


def iterative_pick(e, list):
    '''
    Iteratively samples from list until it finds the index of an
    element e. Returns the number of samplings to find the index of the
    element. Returns None if e is not in list.
    '''
    if e not in list:
        return None
    for i in range(len(list)):
        if list[i] == e:
            return i + 1


def main(list_size=1000, no_lists=1000, no_trials=100):
    '''
    Prints out a comma separated table comparing random_pick and
    iterative_pick for a list of size list_size. The table contains
    no_trials independent trials where each trial consists of the
    average number of samples needed to identify the index of the
    element in no_lists lists.
    '''
    print("trial,random,iterative")
    for trial in range(1, no_trials + 1):
        list = [i for i in range(list_size)]
        random.shuffle(list)
        rnd = 0
        itr = 0
        for _ in range(no_lists):
            e = random.choice(list)
            rnd += random_pick(e, list)
            itr += iterative_pick(e, list)
        print("{},{},{}".format(trial, rnd / no_lists, itr / no_lists))

if __name__ == '__main__':
    main()
